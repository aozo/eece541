#!/bin/bash
#SBATCH --time=120:00:00
#SBATCH --account=def-mahsa77
#SBATCH --job-name=compress_painter
#SBATCH --ntasks=1
#SBATCH --mem-per-cpu=16G
#SBATCH --output=%x-%j.out
#SBATCH --mail-user=<rruiyao@student.ubc.ca>
#SBATCH --mail-type=BEGIN
#SBATCH --mail-type=END
#SBATCH --mail-type=FAIL

./TAppEncoderStatic -c "/project/6060158/541_Sep2021_LFQM/HTM-16.1-dev/bin/cfg_Nusrat/Painter_16view.cfg" -c "/project/6060158/541_Sep2021_LFQM/HTM-16.1-dev/bin/cfg_Nusrat/Painter_45.cfg" -c "/project/6060158/541_Sep2021_LFQM/HTM-16.1-dev/bin/cfg_Nusrat/specsPainter.cfg" >> "/project/6060158/541_Sep2021_LFQM/HTM-16.1-dev/bin/painter/log/painter_qp45.log"
