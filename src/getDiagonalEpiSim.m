% Calculates the average similarity between the diagonal EPIs of the two light
% field images.
%
% @param   views     Cell array of all views from the light field image.
% @param   viewsRef  Cell array of all views from the reference light field image.
% @param   c         Structure containing constants from the caller.
%
% @return  avgPsnr   Average PSNR between the diagonal EPIs.
% @return  avgSsim   Average SSIM between the diagonal EPIs.
function [avgPsnr, avgSsim] = getDiagonalEpiSim(views, viewsRef, c)
    totalPsnr = 0;
    totalSsim = 0;
    totalEpis = 0;

    % List of diagonal views at 135 degrees from the horizontal axis
    % | x |   |   |   |     |   |   |   |   |     |   |   |   |   |
    % |   | x |   |   | --> | x |   |   |   | --> |   |   |   |   |
    % |   |   | x |   |     |   | x |   |   |     | x |   |   |   |
    % |   |   |   | x |     |   |   | x |   |     |   | x |   |   |
    for vRow = 1:c.NUM_VROWS - 1
        rows = vRow:c.NUM_VROWS;
        cols = 1:c.NUM_VCOLS - vRow + 1;
        nDiagViews = length(rows);

        [epis, ~] = getDiagonalEpi(views, rows, cols, c);
        [episRef, ~] = getDiagonalEpi(viewsRef, rows, cols, c);

        idx = 1;
        for iRow = 1:c.HEIGHT
            epi = uint8(epis(idx:idx+nDiagViews-1, :));
            epiRef = uint8(episRef(idx:idx+nDiagViews-1, :));

            % Only calculate the similarity if the EPI exists in both arrays.
            if (sum(epi(:)) > 0) && (sum(epiRef(:)) > 0)
                p = psnr(epi, epiRef);
                totalPsnr = totalPsnr + p;

                s = ssim(epi, epiRef);
                totalSsim = totalSsim + s;

                totalEpis = totalEpis + 1;
            end

            idx = idx + nDiagViews;
        end
    end

    % |   | x |   |   |     |   |   | x |   |
    % |   |   | x |   | --> |   |   |   | x |
    % |   |   |   | x |     |   |   |   |   |
    % |   |   |   |   |     |   |   |   |   |
    for vCol = 2:c.NUM_VCOLS - 1
        rows = 1:c.NUM_VROWS - vCol + 1;
        cols = vCol:c.NUM_VCOLS;
        nDiagViews = length(rows);

        [epis, ~] = getDiagonalEpi(views, rows, cols, c);
        [episRef, ~] = getDiagonalEpi(viewsRef, rows, cols, c);

        idx = 1;
        for iRow = 1:c.HEIGHT
            epi = uint8(epis(idx:idx+nDiagViews-1, :));
            epiRef = uint8(episRef(idx:idx+nDiagViews-1, :));

            % Only calculate the similarity if the EPI exists in both arrays.
            if (sum(epi(:)) > 0) && (sum(epiRef(:)) > 0)
                p = psnr(epi, epiRef);
                totalPsnr = totalPsnr + p;

                s = ssim(epi, epiRef);
                totalSsim = totalSsim + s;

                totalEpis = totalEpis + 1;
            end

            idx = idx + nDiagViews;
        end
    end

    % List of diagonal views at 45 degrees from the horizontal axis
    % |   |   |   | x |     |   |   |   |   |     |   |   |   |   |
    % |   |   | x |   | --> |   |   |   | x | --> |   |   |   |   |
    % |   | x |   |   |     |   |   | x |   |     |   |   |   | x |
    % | x |   |   |   |     |   | x |   |   |     |   |   | x |   |
    for vRow = 1:c.NUM_VROWS - 1
        rows = vRow:c.NUM_VROWS;
        cols = c.NUM_VCOLS:-1:vRow;
        nDiagViews = length(rows);

        [epis, ~] = getDiagonalEpi(views, rows, cols, c);
        [episRef, ~] = getDiagonalEpi(viewsRef, rows, cols, c);

        idx = 1;
        for iRow = 1:c.HEIGHT
            epi = uint8(epis(idx:idx+nDiagViews-1, :));
            epiRef = uint8(episRef(idx:idx+nDiagViews-1, :));

            % Only calculate the similarity if the EPI exists in both arrays.
            if (sum(epi(:)) > 0) && (sum(epiRef(:)) > 0)
                p = psnr(epi, epiRef);
                totalPsnr = totalPsnr + p;

                s = ssim(epi, epiRef);
                totalSsim = totalSsim + s;

                totalEpis = totalEpis + 1;
            end

            idx = idx + nDiagViews;
        end
    end

    % |   |   | x |   |     |   | x |   |   |
    % |   | x |   |   | --> | x |   |   |   |
    % | x |   |   |   |     |   |   |   |   |
    % |   |   |   |   |     |   |   |   |   |
    for vCol = c.NUM_VCOLS - 1:-1:2
        rows = 1:vCol;
        cols = vCol:-1:1;
        nDiagViews = length(rows);

        [epis, ~] = getDiagonalEpi(views, rows, cols, c);
        [episRef, ~] = getDiagonalEpi(viewsRef, rows, cols, c);

        idx = 1;
        for iRow = 1:c.HEIGHT
            epi = uint8(epis(idx:idx+nDiagViews-1, :));
            epiRef = uint8(episRef(idx:idx+nDiagViews-1, :));

            % Only calculate the similarity if the EPI exists in both arrays.
            if (sum(epi(:)) > 0) && (sum(epiRef(:)) > 0)
                p = psnr(epi, epiRef);
                totalPsnr = totalPsnr + p;

                s = ssim(epi, epiRef);
                totalSsim = totalSsim + s;

                totalEpis = totalEpis + 1;
            end

            idx = idx + nDiagViews;
        end
    end

    avgPsnr = totalPsnr / totalEpis;
    avgSsim = totalSsim / totalEpis;
end
