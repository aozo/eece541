% Imports the light field .yuv views from an input folder. Assumes file names
% are numbered in raster order.
%
% e.g.
% | View01 View02 View03 |
% | View04 View05 View06 |
% | View07 View08 View09 |
%
% @param   folder  Folder name string relative from the current directory. e.g.
%                  'dataset/painter'
% @param   c       Structure containing constants from the caller.
%
% @return  views   Cell array containing the Y component of each YUV video in
%                  the light field video. The size of the cell array is the same
%                  as the number of views in the light field video. the diagonal
%                  EPI.
% @return  files   Array of file names.
function [views, files] = importViews(folder, c)
    path = fullfile(folder, '*.yuv');
    files = dir(path);

    % Reshape light field views into 2D array.
    % Take the transpose of the final array to convert into the following:
    % | View01 View04 View07 |    | View01 View02 View03 |
    % | View02 View05 View08 | -> | View04 View05 View06 |
    % | View03 View06 View09 |    | View07 View08 View09 |
    files = transpose(reshape(files, [c.NUM_VROWS, c.NUM_VCOLS]));

    % Import YUV videos
    for vRow = 1:c.NUM_VROWS
        for vCol = 1:c.NUM_VCOLS
            fileName = files(vRow, vCol).name;
            vid = fullfile(folder, fileName);

            % Import raw YUV files
            [Y, U, V] = yuv_import(vid, [c.WIDTH, c.HEIGHT], c.FRAME, 0, 'YUV420_8');
            views{vRow, vCol} = Y{1, c.FRAME};
        end
    end
end
