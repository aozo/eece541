% Generates an array of EPIs and gradients for a single diagonal.
%
% @param   views    Cell array of all views from the light field image.
% @param   rows     Array containing the row indices of the views in the
%                   diagonal.
% @param   cols     Array containing the column indices of the views in the
%                   diagonal.
% @param   c        Structure containing constants from the caller.
%
% @return  epis     Array of all diagonal EPIs. Since some diagonal EPIs are not
%                   valid after compression, this array has a preallocated size
%                   of (nDiagViews * c.NUM_VROWS, c.WIDTH) and for rows where a
%                   diagonal EPI is not valid, the corresponding entries in the
%                   array are left as zero. This is necessary to calculate the
%                   similarity between the diagonal EPIs of two images.
% @return  grads    Array of gradients for all diagonal EPIs.
function [epis, grads] = getDiagonalEpi(views, rows, cols, c)
    % rows and cols contain list of indices of views belonging in the diagonal
    nDiagViews = size(rows, 2);
    avgOffsets = zeros(nDiagViews - 1, 1);

    % Only display figures for longest diagonals
    display = c.ENABLE_SAMPLE_EPI && (nDiagViews == size(views, 1));

    % Calculate offsets between consecutive pairs of views
    if display
        figure;
    end

    for v = 2:nDiagViews
        view1 = uint8(views{rows(v - 1), cols(v - 1)});
        view2 = uint8(views{rows(v), cols(v)});

        % Detect SIFT features in each view of the pair
        points1 = detectSIFTFeatures(view1);
        points2 = detectSIFTFeatures(view2);
        [features1, validPoints1] = extractFeatures(view1, points1);
        [features2, validPoints2] = extractFeatures(view2, points2);

        % Match features to determine the spatial offset
        indexPairs = matchFeatures(features1, features2);
        matchedPoints1 = validPoints1(indexPairs(:, 1), :);
        matchedPoints2 = validPoints2(indexPairs(:, 2), :);

        % Determine the inliers of the matched features
        [~, inliers] = estimateFundamentalMatrix(matchedPoints1, matchedPoints2);
        inlierPoints1 = matchedPoints1(inliers).Location;
        inlierPoints2 = matchedPoints2(inliers).Location;

        % Calculate the (x, y) delta between each matched feature
        offsets = inlierPoints2 - inlierPoints1;

        % Calculate the average vertical offset
        avgOffsets(v - 1) = mean(offsets(:, 2));

        if display
            subplot(nDiagViews - 1, 1, v - 1);
            showMatchedFeatures(view1, ...
                view2, ...
                matchedPoints1(inliers, :), ...
                matchedPoints2(inliers, :));
            title(['View(', num2str(rows(v - 1)), ', ', ...
                num2str(cols(v - 1)), ') (Red) vs. View(', num2str(rows(v)), ...
                ', ', num2str(cols(v)), ') (Green)']);
        end
    end

    if display
        figure;

        for vRow = 1:c.NUM_VROWS
            for vCol = 1:c.NUM_VCOLS
                curView = views{vRow, vCol};

                subplot(c.NUM_VROWS, ...
                    c.NUM_VCOLS, ...
                    sub2ind([c.NUM_VROWS, c.NUM_VCOLS], vCol, vRow));
                imshow(curView, []);
            end
        end

        % Highlight the first row in the diagonal EPI
        curView = views{rows(1), cols(1)};
        curView(c.MIN_IROW:c.MAX_IROW, :) = 255;

        subplot(c.NUM_VROWS, ...
            c.NUM_VCOLS, ...
            sub2ind([c.NUM_VROWS, c.NUM_VCOLS], cols(1), rows(1)));
        imshow(uint8(curView), []);

        % Highlight the rows in the subsequent diagonal views.
        minRow = c.MIN_IROW;
        maxRow = c.MAX_IROW;
        for v = 2:nDiagViews
            curView = views{rows(v), cols(v)};
            minRow = minRow + avgOffsets(v - 1);
            maxRow = maxRow + avgOffsets(v - 1);
            curView(int64(minRow):int64(maxRow), :) = 255;

            subplot(c.NUM_VROWS, ...
                c.NUM_VCOLS, ...
                sub2ind([c.NUM_VROWS, c.NUM_VCOLS], cols(v), rows(v)));
            imshow(uint8(curView), []);
        end
        sgtitle('Sample Diagonal EPI');
    end

    % Determine how many valid diagonal EPIs there are to preallocate the
    % gradients array.
    numEpis = 0;
    for iRow = 1:c.HEIGHT
        % Determine if the target row is still within the image after adding
        % the total vertical offset.
        maxMinOffset = iRow + sum(avgOffsets);
        if (maxMinOffset >= 1) && (maxMinOffset <= c.HEIGHT)
            numEpis = numEpis + 1;
        end
    end

    % Generate the diagonal EPI
    epi = uint8(zeros(nDiagViews, c.WIDTH));
    epis = zeros(c.HEIGHT * nDiagViews, c.WIDTH);
    grads = zeros(numEpis * nDiagViews, c.WIDTH);

    epiIdx = 1;
    gradIdx = 1;
    for iRow = 1:c.HEIGHT
        % Determine if the target row is still within the image after adding
        % the total vertical offset.
        maxMinOffset = iRow + sum(avgOffsets);
        if (maxMinOffset >= 1) && (maxMinOffset <= c.HEIGHT)

            % Row from the first diagonal view is not offset
            epi(1, :) = views{rows(1), cols(1)}(iRow, :);

            % Rows from subsequent diagonal views are offset by the average
            % calculated from matched features.
            row = iRow;
            for vDiag = 2:nDiagViews
                row = row + avgOffsets(vDiag - 1);
                epi(vDiag, :) = views{rows(vDiag), cols(vDiag)}(int64(row), :);
            end

            % Only populate the epis array if the diagonal EPI is valid.
            % Otherwise, left as zero.
            epis(epiIdx:epiIdx+nDiagViews-1, :) = epi;

            if display && (iRow == c.SAMPLE_IROW)
                figure;
                imshow(epi, []);
                title('Diagonal EPI');
            end

            % Calculate the gradient direction at every pixel of the diagonal EPI
            [~, grad] = imgradient(epi, 'sobel');
            grads(gradIdx:gradIdx+nDiagViews-1, :) = grad;
            gradIdx = gradIdx + nDiagViews;
        end

        epiIdx = epiIdx + nDiagViews;
    end
end
