% Generates an array of EPIs and gradients for all horizontal EPIs.
%
% @param   views  Cell array of all views from the light field image.
% @param   c      Structure containing constants from the caller.
%
% @return  epis   Array of all horizontal EPIs.
% @return  grads  Array of gradients for all horizontal EPIs.
function [epis, grads] = getHorizontalEpi(views, c)
    % Generate horizontal EPI for every row across all views
    epi = uint8(zeros(c.NUM_VCOLS, c.WIDTH));
    epis = zeros(c.HEIGHT * c.NUM_VROWS * c.NUM_VCOLS, c.WIDTH);
    grads = zeros(c.HEIGHT * c.NUM_VROWS * c.NUM_VCOLS, c.WIDTH);

    idx = 1;
    for vRow = 1:c.NUM_VROWS
        for iRow = 1:c.HEIGHT
            % For each image row, generate the horizontal EPI that spans across
            % all columns of views.
            for vCol = 1:c.NUM_VCOLS
                epi(vCol, :) = views{vRow, vCol}(iRow, :);
            end

            epis(idx:idx+c.NUM_VCOLS-1, :) = epi;

            if c.ENABLE_SAMPLE_EPI && (vRow == c.SAMPLE_VROW) && (iRow == c.SAMPLE_IROW)
                figure;
                imshow(epi, []);
                title('Horizontal EPI');
            end

            % Calculate the gradient direction at every pixel of the horizontal
            % EPI
            [~, grad] = imgradient(epi, 'sobel');
            grads(idx:idx+c.NUM_VCOLS-1, :) = grad;
            idx = idx + c.NUM_VCOLS;
        end
    end
end
