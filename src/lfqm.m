close all; clear all; clc;

% Imports
addpath('YUV');

% Configurations
c.ENABLE_SAMPLE_EPI = false; % Display single EPI for demos/figures
c.ENABLE_HISTOGRAM_METHOD = true; % Metric using kurtosis of gradient histogram
c.ENABLE_SIM_METHOD = true; % Metric using EPI similarities

% Video information (Specific to videos being imported)
c.WIDTH  = 2048; % Video width
c.HEIGHT = 1088; % Video height
% c.FRAME = 372; % Number of frames obtained from compressing using ffmpeg
c.FRAME = 1; % Default to 1 to speed up processing time.
c.NUM_VROWS = 4; % Number of rows of videos in the light field content.
c.NUM_VCOLS = 4; % Number of columns of videos in the light field content.

% EPI constants if ENABLE_SAMPLE_EPI is true
c.SAMPLE_VROW = 2;
c.SAMPLE_IROW = 500;
c.MIN_IROW = max([1, c.SAMPLE_IROW - 10]);
c.MAX_IROW = min([c.HEIGHT, c.SAMPLE_IROW + 10]);

c.SAMPLE_VCOL = 2;
c.SAMPLE_ICOL = 1000;
c.MIN_ICOL = max([1, c.SAMPLE_ICOL - 10]);
c.MAX_ICOL = min([c.WIDTH, c.SAMPLE_ICOL + 10]);

% Videos to import
% Specify folder containing the reference videos and compressed videos.
[viewsRef, filesRef] = importViews('dataset/painter', c);
[viewsQP45, ~] = importViews('dataset/painter_qp_45', c);
[viewsQP40, ~] = importViews('dataset/painter_qp_40', c);
[viewsQP35, ~] = importViews('dataset/painter_qp_35', c);
[viewsQP30, ~] = importViews('dataset/painter_qp_30', c);
[viewsQP25, ~] = importViews('dataset/painter_qp_25', c);

if c.ENABLE_SAMPLE_EPI
    figure;
    for vRow = 1:c.NUM_VROWS
        for vCol = 1:c.NUM_VCOLS
            curView = viewsRef{vRow, vCol};

            % Add a visual indcator of which rows and columns are used to
            % generate the EPIs. Since the content is high resolution, a visual
            % indicator with a width of a single pixel may not always be
            % visible, so a range is used instead.
            if vRow == c.SAMPLE_VROW
                curView(c.MIN_IROW:c.MAX_IROW, :) = 255;
            end

            if vCol == c.SAMPLE_VCOL
                curView(:, c.MIN_ICOL:c.MAX_ICOL) = 255;
            end

            % Note that vRow and vCol are swapped so that subplots are plotted
            % row by row instead of column by column.
            subplot(c.NUM_VROWS, c.NUM_VCOLS, sub2ind([c.NUM_VROWS, c.NUM_VCOLS], vCol, vRow));
            imshow(curView, []);
            title(filesRef(vRow, vCol).name);
        end
    end
    sgtitle('Sample Horizontal and Vertical EPI');
end

if c.ENABLE_HISTOGRAM_METHOD
    figure;
    edges = -180:10:180;

    [~, hGradsRef] = getHorizontalEpi(viewsRef, c);
    h1 = histcounts(hGradsRef(:), edges);

    [~, hGrads25] = getHorizontalEpi(viewsQP25, c);
    h2 = histcounts(hGrads25(:), edges);

    [~, hGrads30] = getHorizontalEpi(viewsQP30, c);
    h3 = histcounts(hGrads30(:), edges);

    [~, hGrads35] = getHorizontalEpi(viewsQP35, c);
    h4 = histcounts(hGrads35(:), edges);

    [~, hGrads40] = getHorizontalEpi(viewsQP40, c);
    h5 = histcounts(hGrads40(:), edges);

    [~, hGrads45] = getHorizontalEpi(viewsQP45, c);
    h6 = histcounts(hGrads45(:), edges);

    subplot(1, 3, 1);
    bar(edges(1:end-1), [h1; h2; h3; h4; h5; h6]');
    legend('Reference', 'QP25', 'QP30', 'QP35', 'QP40', 'QP45');
    title('Histogram of Horizontal EPI Gradients');
    xlabel('Gradient Direction Measured Counterclockwise from the Positive X-Axis (Degrees)');
    ylabel('Count');

    [~, vGradsRef] = getVerticalEpi(viewsRef, c);
    h1 = histcounts(vGradsRef(:), edges);

    [~, vGrads25] = getVerticalEpi(viewsQP25, c);
    h2 = histcounts(vGrads25(:), edges);

    [~, vGrads30] = getVerticalEpi(viewsQP30, c);
    h3 = histcounts(vGrads30(:), edges);

    [~, vGrads35] = getVerticalEpi(viewsQP35, c);
    h4 = histcounts(vGrads35(:), edges);

    [~, vGrads40] = getVerticalEpi(viewsQP40, c);
    h5 = histcounts(vGrads40(:), edges);

    [~, vGrads45] = getVerticalEpi(viewsQP45, c);
    h6 = histcounts(vGrads45(:), edges);

    subplot(1, 3, 2);
    bar(edges(1:end-1), [h1; h2; h3; h4; h5; h6]');
    legend('Reference', 'QP25', 'QP30', 'QP35', 'QP40', 'QP45');
    title('Histogram of Vertical EPI Gradients');
    xlabel('Gradient Direction Measured Counterclockwise from the Positive X-Axis (Degrees)');
    ylabel('Count');

    dGradsRef = getDiagonalEpiGrads(viewsRef, c);
    h1 = histcounts(dGradsRef(:), edges);

    dGrads25 = getDiagonalEpiGrads(viewsQP25, c);
    h2 = histcounts(dGrads25(:), edges);

    dGrads30 = getDiagonalEpiGrads(viewsQP30, c);
    h3 = histcounts(dGrads30(:), edges);

    dGrads35 = getDiagonalEpiGrads(viewsQP35, c);
    h4 = histcounts(dGrads35(:), edges);

    dGrads40 = getDiagonalEpiGrads(viewsQP40, c);
    h5 = histcounts(dGrads40(:), edges);

    dGrads45 = getDiagonalEpiGrads(viewsQP45, c);
    h6 = histcounts(dGrads45(:), edges);

    subplot(1, 3, 3);
    bar(edges(1:end-1), [h1; h2; h3; h4; h5; h6]');
    legend('Reference', 'QP25', 'QP30', 'QP35', 'QP40', 'QP45');
    title('Histogram of Diagonal EPI Gradients');
    xlabel('Gradient Direction Measured Counterclockwise from the Positive X-Axis (Degrees)');
    ylabel('Count');

    hKurtRef = kurtosis(hGradsRef(:));
    vKurtRef = kurtosis(vGradsRef(:));
    dKurtRef = kurtosis(dGradsRef(:));
    avgKurtRef = mean([hKurtRef, vKurtRef, dKurtRef]);

    hKurt45 = kurtosis(hGrads45(:));
    vKurt45 = kurtosis(vGrads45(:));
    dKurt45 = kurtosis(dGrads45(:));
    avgKurt45 = mean([hKurt45, vKurt45, dKurt45]);

    hKurt40 = kurtosis(hGrads40(:));
    vKurt40 = kurtosis(vGrads40(:));
    dKurt40 = kurtosis(dGrads40(:));
    avgKurt40 = mean([hKurt40, vKurt40, dKurt40]);

    hKurt35 = kurtosis(hGrads35(:));
    vKurt35 = kurtosis(vGrads35(:));
    dKurt35 = kurtosis(dGrads35(:));
    avgKurt35 = mean([hKurt35, vKurt35, dKurt35]);

    hKurt30 = kurtosis(hGrads30(:));
    vKurt30 = kurtosis(vGrads30(:));
    dKurt30 = kurtosis(dGrads30(:));
    avgKurt30 = mean([hKurt30, vKurt30, dKurt30]);

    hKurt25 = kurtosis(hGrads25(:));
    vKurt25 = kurtosis(vGrads25(:));
    dKurt25 = kurtosis(dGrads25(:));
    avgKurt25 = mean([hKurt25, vKurt25, dKurt25]);
end

if c.ENABLE_SIM_METHOD
    [hPsnr45, hSsim45] = getHorizontalEpiSim(viewsRef, viewsQP45, c);
    [vPsnr45, vSsim45] = getVerticalEpiSim(viewsRef, viewsQP45, c);
    [dPsnr45, dSsim45] = getDiagonalEpiSim(viewsRef, viewsQP45, c);
    avgPsnr45 = mean([hPsnr45, vPsnr45, dPsnr45]);
    avgSsim45 = mean([hSsim45, vSsim45, dSsim45]);

    [hPsnr40, hSsim40] = getHorizontalEpiSim(viewsRef, viewsQP40, c);
    [vPsnr40, vSsim40] = getVerticalEpiSim(viewsRef, viewsQP40, c);
    [dPsnr40, dSsim40] = getDiagonalEpiSim(viewsRef, viewsQP40, c);
    avgPsnr40 = mean([hPsnr40, vPsnr40, dPsnr40]);
    avgSsim40 = mean([hSsim40, vSsim40, dSsim40]);

    [hPsnr35, hSsim35] = getHorizontalEpiSim(viewsRef, viewsQP35, c);
    [vPsnr35, vSsim35] = getVerticalEpiSim(viewsRef, viewsQP35, c);
    [dPsnr35, dSsim35] = getDiagonalEpiSim(viewsRef, viewsQP35, c);
    avgPsnr35 = mean([hPsnr35, vPsnr35, dPsnr35]);
    avgSsim35 = mean([hSsim35, vSsim35, dSsim35]);

    [hPsnr30, hSsim30] = getHorizontalEpiSim(viewsRef, viewsQP30, c);
    [vPsnr30, vSsim30] = getVerticalEpiSim(viewsRef, viewsQP30, c);
    [dPsnr30, dSsim30] = getDiagonalEpiSim(viewsRef, viewsQP30, c);
    avgPsnr30 = mean([hPsnr30, vPsnr30, dPsnr30]);
    avgSsim30 = mean([hSsim30, vSsim30, dSsim30]);

    [hPsnr25, hSsim25] = getHorizontalEpiSim(viewsRef, viewsQP25, c);
    [vPsnr25, vSsim25] = getVerticalEpiSim(viewsRef, viewsQP25, c);
    [dPsnr25, dSsim25] = getDiagonalEpiSim(viewsRef, viewsQP25, c);
    avgPsnr25 = mean([hPsnr25, vPsnr25, dPsnr25]);
    avgSsim25 = mean([hSsim25, vSsim25, dSsim25]);
end

