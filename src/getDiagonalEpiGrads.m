% Calculates the gradient of all diagonal EPIs.
%
% @param   views  Cell array of all views from the light field image.
% @param   c      Structure containing constants from the caller.
%
% @return  grads  Array of gradients for all diagonal EPIs.
function grads = getDiagonalEpiGrads(views, c)
    grads = [];

    % List of diagonal views at 135 degrees from the horizontal axis
    % | x |   |   |   |     |   |   |   |   |     |   |   |   |   |
    % |   | x |   |   | --> | x |   |   |   | --> |   |   |   |   |
    % |   |   | x |   |     |   | x |   |   |     | x |   |   |   |
    % |   |   |   | x |     |   |   | x |   |     |   | x |   |   |
    for vRow = 1:c.NUM_VROWS - 1
        rows = vRow:c.NUM_VROWS;
        cols = 1:c.NUM_VCOLS - vRow + 1;

        [~, g] = getDiagonalEpi(views, rows, cols, c);
        grads = [grads; g];
    end

    % |   | x |   |   |     |   |   | x |   |
    % |   |   | x |   | --> |   |   |   | x |
    % |   |   |   | x |     |   |   |   |   |
    % |   |   |   |   |     |   |   |   |   |
    for vCol = 2:c.NUM_VCOLS - 1
        rows = 1:c.NUM_VROWS - vCol + 1;
        cols = vCol:c.NUM_VCOLS;

        [~, g] = getDiagonalEpi(views, rows, cols, c);
        grads = [grads; g];
    end

    % List of diagonal views at 45 degrees from the horizontal axis
    % |   |   |   | x |     |   |   |   |   |     |   |   |   |   |
    % |   |   | x |   | --> |   |   |   | x | --> |   |   |   |   |
    % |   | x |   |   |     |   |   | x |   |     |   |   |   | x |
    % | x |   |   |   |     |   | x |   |   |     |   |   | x |   |
    for vRow = 1:c.NUM_VROWS - 1
        rows = vRow:c.NUM_VROWS;
        cols = c.NUM_VCOLS:-1:vRow;

        [~, g] = getDiagonalEpi(views, rows, cols, c);
        grads = [grads; g];
    end

    % |   |   | x |   |     |   | x |   |   |
    % |   | x |   |   | --> | x |   |   |   |
    % | x |   |   |   |     |   |   |   |   |
    % |   |   |   |   |     |   |   |   |   |
    for vCol = c.NUM_VCOLS - 1:-1:2
        rows = 1:vCol;
        cols = vCol:-1:1;

        [~, g] = getDiagonalEpi(views, rows, cols, c);
        grads = [grads; g];
    end
end
