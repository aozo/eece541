% Calculates the average similarity between the vertical EPIs of the two light
% field images.
%
% @param   views     Cell array of all views from the light field image.
% @param   viewsRef  Cell array of all views from the reference light field image.
% @param   c         Structure containing constants from the caller.
%
% @return  avgPsnr   Average PSNR between the vertical EPIs.
% @return  avgSsim   Average SSIM between the vertical EPIs.
function [avgPsnr, avgSsim] = getVerticalEpiSim(views, viewsRef, c)
    totalPsnr = 0;
    totalSsim = 0;
    totalEpis = c.WIDTH * c.NUM_VCOLS;

    [epis, ~] = getVerticalEpi(views, c);
    [episRef, ~] = getVerticalEpi(viewsRef, c);

    idx = 1;
    for iCol = 1:totalEpis
        epi = uint8(epis(idx:idx+c.NUM_VROWS-1, :));
        epiRef = uint8(episRef(idx:idx+c.NUM_VROWS-1, :));

        p = psnr(epi, epiRef);
        totalPsnr = totalPsnr + p;

        s = ssim(epi, epiRef);
        totalSsim = totalSsim + s;

        idx = idx + c.NUM_VROWS;
    end

    avgPsnr = totalPsnr / totalEpis;
    avgSsim = totalSsim / totalEpis;
end
