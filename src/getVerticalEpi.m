% Generates an array of EPIS and gradients for all vertical EPIs.
%
% @param   views  Cell array of all views from the light field image.
% @param   c      Structure containing constants from the caller.
%
% @return  epis   Array of all vertical EPIs.
% @return  grads  Array of gradients for all vertical EPIs.
function [epis, grads] = getVerticalEpi(views, c)
    % Generate vertical EPI for every column across all views
    epi = uint8(zeros(c.NUM_VROWS, c.HEIGHT));
    epis = zeros(c.WIDTH * c.NUM_VCOLS * c.NUM_VROWS, c.HEIGHT);
    grads = zeros(c.WIDTH * c.NUM_VCOLS * c.NUM_VROWS, c.HEIGHT);

    idx = 1;
    for vCol = 1:c.NUM_VCOLS
        for iCol = 1:c.WIDTH
            % For each image col, generate the vertical EPI that spans across all
            % rows of views.
            for vRow = 1:c.NUM_VROWS
                epi(vRow, :) = transpose(views{vRow, vCol}(:, iCol));
            end

            epis(idx:idx+c.NUM_VROWS-1, :) = epi;

            if c.ENABLE_SAMPLE_EPI && (vCol == c.SAMPLE_VCOL) && (iCol == c.SAMPLE_ICOL)
                figure;
                imshow(epi, []);
                title('Vertical EPI');
            end

            % Calculate the gradient direction at every pixel of the vertical EPI
            [~, grad] = imgradient(epi, 'sobel');
            grads(idx:idx+c.NUM_VROWS-1, :) = grad;
            idx = idx + c.NUM_VROWS;
        end
    end
end
