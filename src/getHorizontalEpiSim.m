% Calculates the average similarity between the horizontal EPIs of the two light
% field images.
%
% @param   views     Cell array of all views from the light field image.
% @param   viewsRef  Cell array of all views from the reference light field image.
% @param   c         Structure containing constants from the caller.
%
% @return  avgPsnr   Average PSNR between the horizontal EPIs.
% @return  avgSsim   Average SSIM between the horizontal EPIs.
function [avgPsnr, avgSsim] = getHorizontalEpiSim(views, viewsRef, c)
    totalPsnr = 0;
    totalSsim = 0;
    totalEpis = c.HEIGHT * c.NUM_VROWS;

    [epis, ~] = getHorizontalEpi(views, c);
    [episRef, ~] = getHorizontalEpi(viewsRef, c);

    idx = 1;
    for iRow = 1:totalEpis
        epi = uint8(epis(idx:idx+c.NUM_VCOLS-1, :));
        epiRef = uint8(episRef(idx:idx+c.NUM_VCOLS-1, :));

        p = psnr(epi, epiRef);
        totalPsnr = totalPsnr + p;

        s = ssim(epi, epiRef);
        totalSsim = totalSsim + s;

        idx = idx + c.NUM_VCOLS;
    end

    avgPsnr = totalPsnr / totalEpis;
    avgSsim = totalSsim / totalEpis;
end
