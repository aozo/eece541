# README

* Project Name: EECE541: Light Field Quality Metrics
* Members: Dan Jin, Rui Yao, Thomas Dykstra

## Environment

* MATLAB R2021b
* MATLAB toolboxes:
    * Computer Vision
    * Image Processing
    * Statistics and Machine Learning
* Operating System: Windows 11

## Steps to Compress Videos
1. Install HTM-16.1-dev from /project/def-mahsa77/Software on the Canada Compute
   Beluga server.
2. Edit the .cfg files in the cfg/ directory to match the properties of your
   video files (file name, location, resolution, frame rate, frames to encode,
   QP, etc.).
3. Every video in the light field data set should be labeled with a ViewId.
   There are some rules about how to label the views as I, P, and B frames. An
   example layout of the 4x4 Painter data set is the following:

| 8: 0   | 7: 1   | 4: 2  | 9: 3   |
| 2: 4   | 1: 5   | 0: 6  | 3: 7   |
| 11: 8  | 10: 9  | 5: 10 | 14: 11 |
| 13: 12 | 12: 13 | 6: 14 | 15: 16 |

   The first value is the ViewId which defines the encoding order.

4. Edit the batch file to match your credentials and location of your config
   files. Place the batch file in the bin/ directory of the installed
   HTM-16.1-dev software. The one that was used for this project is located in
   compression/compressPainter45.sh.
5. The script can be executed in Compute Canada:

```
sbatch compressPainter45.sh
```

## Steps to Generate Quality Metrics

1. Light field video names are numbered in raster order and placed in the
   src/dataset/ directory. (Videos are assumed to be in .yuv format)
2. Specify videos to import. The script currently requires a reference light
   field video and compressed video to calculate the full-reference quality
   metric.
3. Update main MATLAB script src/lfqm.m with video properties:
    * c.WIDTH
    * c.HEIGHT
    * c.FRAME
    * c.NUM_VROWS
    * c.NUM_VCOLS
4. Set ENABLE_SAMPLE_EPI to true to display sample figures for demos.
5. Set ENABLE_HISTOGRAM_METHOD to true to generate the metric that uses the
   average kurtosis of the EPI gradient histograms.
6. Set ENAGLE_SIM_METHOD to true to generate the metric that uses the average
   similarity (SSIM/PSNR) of the EPIs.
7. Run the src/lfqm.m script from the MATLAB R2021b GUI. Note that this may take
   more than 10 minutes to process depending on the number of compressed videos
   specified.

## External Libraries
* Nikola Sprljan (2021). YUV files reading and converting (https://www.mathworks.com/matlabcentral/fileexchange/36417-yuv-files-reading-and-converting), MATLAB Central File Exchange. Retrieved November 14, 2021.
    * No modifications made to this library. Used for importing YUV videos.
